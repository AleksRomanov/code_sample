import Vue from 'vue';
import Vuex from 'vuex';
import healthcheck from '@/store/modules/health-check.store';
import tickets from '@/store/modules/tickets.store';
import users from '@/store/modules/users.store';
import advertiser from '@/store/modules/advertiser.store';
import dashboard from '@/store/modules/dashboard.store';
import user from '@/store/modules/login.store';
import inn from '@/store/modules/adv-auth.store';
import funnel from '@/store/modules/funnel.store';
import translator from '@/store/modules/translator.store';
import history from '@/store/modules/history.store';
import draftType from '@/store/modules/draft-type.store';
import company from '@/store/modules/company.store';
import notifications from '@/store/modules/notifications.store';

Vue.use(Vuex);
export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    healthcheck,
    notifications,
    tickets,
    users,
    advertiser,
    dashboard,
    user,
    inn,
    funnel,
    translator,
    history,
    draftType,
    company,
  },
});
