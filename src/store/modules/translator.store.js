import axios from 'axios';
import Vue from 'vue';
import { getInitialTranslatorForm } from '@/utils';

export default {
  state: {
    currentTranslator: getInitialTranslatorForm(),
    translators: [],
    draftTranslators: [],
    pendingTranslators: [],
    archiveTranslators: [],
    advertising: {},
    file: {},
    advertisings: [],
    advertisingQuestion: [],
    themes: [],
    setUserThemes: [],
    allThemes: [],
    allFrames: [],
    videoList: [],
    geoList: [],
    currentItem: [],
    currentItemSelected: Number,
    totalCost: [],
    currentTotalCost: [],
    viewCost: [],
    currentViewCost: [],
    currentViewCostMonth: [],
    technicalCost: [],
    currentTechnicalCost: [],
    activeTranslator: [],
    adaptToServerTranslator: [],
    mediaId: [],
    isActiveUserSelection: 'NOT_ACTIVE',
  },
  actions: {
    /* Подгрузка тем по интересам пользователя */
    async getUserThemesAction({ commit }) {
      const url = `${process.env.VUE_APP_BASE_URL}/interests/all`;
      try {
        const allThemes = await axios.get(url, {
          headers: {
            'Access-Control-Allow-Origin': 'http://localhost:8080',
            'Content-Type': 'application/json',
          },
          withCredentials: true,
        });
        // console.log('getUserThemesAction - DONE');
        commit('setUserThemes', allThemes.data);
        // console.log('ALLTHEMES_DATA', allThemes.data);
      } catch (error) {
        console.log('ERROR');
      }
    },
    async createAdvertisingAction({ commit }, [getMediaId, advertising]) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/?id=${getMediaId}`;
      await axios.post(
        url,
        advertising,
        {
          withCredentials: true,
        },
      ).then((response) => {
        commit('setActiveTranslator', response.data.id);
        // console.log('setActiveTranslatorAction', response.data.id);
      }).catch((error) => {
        console.log(error);
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    /* Подгрузка вопросов */
    async getAdvertisingQuestion(context, question) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising`;
      await axios.post(
        url,
        question,
        {
          withCredentials: true,
        },
      ).then((advertisingQuestion) => {
        // console.log('!!!getAdvertisingQuestion - STORE!!!', advertisingQuestion.data.question);
        context.commit('setAdvertisingQuestion', advertisingQuestion.data.question);
      }).catch((error) => {
        console.log(error);
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    /* Подгрузка фрэймов */
    async getAdvVideoFramesAction({ commit }, id) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/media/frames/extract`;
      try {
        const { data } = await axios.get(url, {
          params: {
            id,
          },
          headers: {
            'Access-Control-Allow-Origin': 'http://localhost:8080',
            'Content-Type': 'application/json',
          },
          withCredentials: true,
        });
        commit('setVideoFrames', data);
      } catch (error) {
        console.log('ERROR');
      }
    },
    /* Подгрузка отфильтрованных трансляторов */
    async getVideoListAction({ commit }, status) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/status?is_=${status}`;
      try {
        const { data } = await axios.get(url, {
          headers: {
            'Access-Control-Allow-Origin': 'http://localhost:8080',
            'Content-Type': 'application/json',
          },
          withCredentials: true,
        });
        // console.log('Подгрузка отфильтрованных трансляторов -', data);
        commit('setVideoList', data);
      } catch (error) {
        console.log('ERROR');
      }
    },
    /* Подгрузка географии показа */
    async getAddressGeoList({ commit }, query) {
      const res = await axios.get('/api/v1/dadata/address/hints', {
        params: {
          query,
        },
      });
      commit('setGeoList', res.data);
      // console.log('setGeoList-Action', res.data);
      return res.data;
    },
    async uploadAdvertisingQuizAction(context, [info, quiz]) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising`;
      const data = {
        quiz_json: quiz,
        advert: info,
      };
      await axios.post(
        url,
        data,
        {
          withCredentials: true,
          'Content-Type': 'application/json',
        },
      ).then((response) => {
        context.commit('setAdvertising', response.data);
      }).catch((error) => {
        Vue.$vToastify.error({
          body: `Advertising upload error: ${error}`,
        });
      });
    },
    async getAdvertisingsAction(context) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/`;
      await axios.get(url, { withCredentials: true }).then((response) => {
        context.commit('setAdvertisings', response.data);
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    async getAdvActiveAdvertisingsAction(context, isActive) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/active`;
      await axios.get(
        url,
        {
          withCredentials: true,
          params: {
            is_active: isActive,
          },
        },
      ).then((response) => {
        context.commit('setAdvertisings', response.data);
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    async getAdvAdvertisingAction(context, id) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/${id}`;
      await axios.get(
        url,
      ).then((response) => {
        context.commit('setAdvertising', response.data);
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    async updateAdvertisingAction(context, [id, info, quiz]) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/`;
      const data = {
        quiz_json: quiz,
        advert: info,
      };
      await axios.put(
        url,
        data,
        {
          withCredentials: true,
          params: {
            id,
          },
        },
      ).then((response) => {
        context.commit('setAdvertising', response.data);
      }).catch((error) => {
        Vue.$vToastify.error({
          body: `Error with advertising updating:${error}`,
        });
      });
    },
    async updateAdvertisingInfoAction(context, [id, data]) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/info/`;
      await axios(
        {
          method: 'put',
          url,
          data,
          withCredentials: true,
          params: {
            id,
          },
          headers: {
            'Content-Type': 'application/json',
          },
        },
      ).then((response) => {
        context.commit('setAdvertising', response.data);
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    async updateAdvertisingVideoAction(context, [id, file]) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/video/`;
      const data = new FormData();
      data.append('file', file);

      await axios(
        {
          method: 'PUT',
          url,
          data,
          withCredentials: true,
          params: {
            id,
          },
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        },
      ).then((response) => {
        Vue.$vToastify.success({
          body: response.data,
        });
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    async changeIsActiveAdvertisingsAction(context, [id, isActive]) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/active`;
      const params = {
        id,
        is_active: isActive,
      };
      await axios(
        {
          method: 'PUT',
          url,
          withCredentials: true,
          params,
        },
      ).then(() => {
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    async deleteAdvertisingAction(context, id) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/advertising/`;
      await axios.delete(
        url,
        {
          withCredentials: true,
          params: {
            id,
          },
        },
      ).then(() => {
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    setMediaIdAction(context, data) {
      // console.log('setMediaIdAction', data);
      context.commit('setMediaId', data);
    },
    setCurrentTranslatorAction(context, data) {
      context.commit('setCurrentTranslator', data);
    },
    setNewTranslatorAction(context, data) {
      context.commit('activeTranslator', data);
    },
    setUserThemesAction(context, data) {
      context.commit('setUserThemes', data);
    },
    setCurrentFrameAction(context, currentItem) {
      context.commit('setCurrentItem', currentItem);
    },
    setTotalCostAction(context, totalCost) {
      context.commit('setTotalCost', totalCost);
    },
    setViewCostAction(context, viewCost) {
      // console.log('setViewCostAction - STORE', viewCost);
      context.commit('setViewCost', viewCost);
    },
    setViewCostMonthAction(context, currentViewCostMonth) {
      // console.log('setViewCostAction - STORE', currentViewCostMonth);
      context.commit('setViewCostMonth', currentViewCostMonth);
    },
    setTechnicalCostAction(context, technicalCost) {
      context.commit('setTechnicalCost', technicalCost);
    },
    setUserQuestions(context, data) {
      context.commit('setAdvertisingQuestions', data);
    },
    addToArchiveTranslatorsAction(context, data) {
      console.log('STORE-addToArchiveTranslatorsAction', data);
      context.commit('addToArchiveTranslators', data);
      context.commit('deleteInTranslators', data);
    },
    refreshTranslatorAction(context, data) {
      context.commit('addToTranslators', data);
      context.commit('deleteInArchiveTranslators', data);
    },
    deleteTranslatorAction(context, data) {
      context.commit('deleteInArchiveTranslators', data);
    },
    addDraftTranslatorsAction(context) {
      context.commit('addDraftTranslators');
    },
  },
  mutations: {
    setCurrentItem(state, currentItem) {
      state.currentItemSelected = currentItem;
    },
    setTotalCost(state, totalCost) {
      state.currentTotalCost = totalCost;
    },
    setViewCost(state, viewCost) {
      state.currentViewCost = viewCost;
    },
    setViewCostMonth(state, currentViewCostMonth) {
      state.currentViewCostMonth = currentViewCostMonth;
    },
    setTechnicalCost(state, technicalCost) {
      state.currentTechnicalCost = technicalCost;
    },
    setMediaId(state, mediaId) {
      state.mediaId = mediaId;
    },
    setVideoFrames(state, allFrames) {
      state.allFrames = allFrames;
    },
    setVideoList(state, vlist) {
      state.videoList = vlist;
    },
    setActiveTranslator(state, activeTranslator) {
      state.activeTranslator = activeTranslator;
    },
    setUserThemes(state, allThemes) {
      state.allThemes = allThemes;
    },
    setAdvertising(state, advertising) {
      state.advertising = advertising;
    },
    setGeoList(state, dataAddress) {
      state.geoList = dataAddress;
    },
    setAdvertisingQuestion(state, advertisingQuestion) {
      state.advertising = advertisingQuestion;
    },
    setFile(state, file) {
      state.file = file;
    },
    setAdvertisings(state, advertisings) {
      state.advertisings = advertisings;
    },
    setCurrentTranslator(state, data) {
      state.currentTranslator = data;
    },
    addToArchiveTranslators(state, data) {
      state.archiveTranslators.push(data);
    },
    deleteInTranslators(state, data) {
      const index = state.translators.findIndesetGeoListx((item) => item.id === data.id);
      state.translators.splice(index, 1);
    },
    deleteInArchiveTranslators(state, data) {
      const index = state.archiveTranslators.findIndex((item) => item.id === data.id);
      state.archiveTranslators.splice(index, 1);
    },
    addToTranslators(state, data) {
      state.translators.push(data);
    },
    addDraftTranslators(state) {
      state.draftTranslators.push(state.currentTranslator);
    },
  },
  getters: {
    getAdvertisingQuestion(state) {
      return state.advertising;
    },
    getActiveTranslator(state) {
      // console.log('getters - getActiveTranslator(state)', state.translators);
      return state.translators;
    },
    getMediaId(state) {
      // console.log('STORE-GETTERS-getMediaId -', state.mediaId);
      return state.mediaId;
    },
    getVideoFrames(state) {
      return state.allFrames;
    },
    getCurrentFrame(state) {
      return {
        currentItem: state.currentItemSelected,
        frameUrl: state.allFrames[state.currentItemSelected],
      };
    },
    getTotalCost(state) {
      return state.currentTotalCost;
    },
    getViewCost(state) {
      return state.currentViewCost;
    },
    getViewCostMonth(state) {
      return state.currentViewCostMonth;
    },
    getTechnicalCost(state) {
      // console.log('GETTER', state.technicalCost);
      return state.currentTechnicalCost;
    },
    getUserThemes(state) {
      return state.allThemes;
    },
    getAddressGeo(state) {
      return state.geoList;
    },
    getCurrentTranslator(state) {
      return state.currentTranslator;
    },
    getAdaptToServerTranslator(state) {
      const adaptInterests = (lst) => {
        const result = [];
        for (let i = 0; i < lst.length; i += 1) {
          result.push(lst[i].name);
        }
        return result;
      };
      return {
        name: state.currentTranslator.formName,
        href: null,
        contract: null,
        question: {
          question: state.currentTranslator.questions.value[0].question,
          answers: [
            {
              text: state.currentTranslator.questions.value[0].answer1.value,
              answer: state.currentTranslator.questions.value[0].answer1.status,
            },
            {
              text: state.currentTranslator.questions.value[0].answer2.value,
              answer: state.currentTranslator.questions.value[0].answer2.status,
            },
            {
              text: state.currentTranslator.questions.value[0].answer3.value,
              answer: state.currentTranslator.questions.value[0].answer3.status,
            },
          ],
        },
        gender: state.currentTranslator.gender.value,
        age_from: state.currentTranslator.ages.value.from,
        age_to: state.currentTranslator.ages.value.to,
        education: state.currentTranslator.education.value,
        income: state.currentTranslator.income.value,
        marital_status: state.currentTranslator.martialStatus.value,
        children: null,
        animals: state.currentTranslator.animals.value,
        religion: state.currentTranslator.religion.value,
        interests: adaptInterests(state.currentTranslator.theme.value),
        // interests: state.currentTranslator.theme.value,
        geolocation: state.currentTranslator.geolocationName.value,
        showing_time_of_the_day: state.currentTranslator.timeOfTheDay.value,
        support_period: 30,
        number_of_views: state.currentViewCostMonth,
        price_for_views: state.currentViewCost,
        price_for_click: 2,
        total_price: state.currentTotalCost,
      };
    },
    getTranslators(state) {
      return state.translators;
    },
    getDraftTranslators(state) {
      return state.draftTranslators;
    },
    getVideoList(state) {
      return state.videoList;
    },
    getArchiveTranslators(state) {
      return state.archiveTranslators;
    },
    getAdvertising(state) {
      return state.advertising;
    },
    getFile(state) {
      return state.file;
    },
    getAdvertisings(state) {
      return state.advertisings;
    },
  },
  modules: {},
};
