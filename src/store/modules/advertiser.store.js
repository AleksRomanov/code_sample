import axios from 'axios';
import Vue from 'vue';

export default {
  state: {
    advertiser: {},
    advUser: {},
    advUsers: [],
    company: {},
  },
  actions: {
    /* Подгрузка компании */
    // async getCompanyAction(context) {
    //   const url = `${process.env.VUE_APP_BASE_URL}/adv/user/all`;
    //   await axios.get(url, {
    //     withCredentials: true,
    //   }).then((response) => {
    //     const company = {
    //       name: {
    //         short: 'УРАБИ',
    //       },
    //       inn: '2466277548',
    //       ogrn: '1142468061922',
    //       bank: '',
    //       bankBiK: '',
    //       bankAccount: '',
    //       correspondentCheck: '',
    //       email: '',
    //       phone: '',
    //       fio: 'Григоренко Александр Анатольевич',
    //       users: response.data,
    //     };
    //     console.log('RESPONSE_DATA', company.users);
    //     // context.commit('setCompany', company);
    //     context.commit('setAdvertiser', company);
    //   }).catch((error) => {
    //     Vue.$vToastify.error({
    //       body: error,
    //     });
    //   });
    // },
    async getAdvertiserAction(context) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv`;
      await axios.get(
        url,
        {
          withCredentials: true,
        },
      ).then((response) => {
        context.commit('setAdvertiser', response.data);
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    async getAdvertiserAllUsersAction(context) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/user/all`;
      await axios.get(
        url,
        {
          withCredentials: true,
        },
      ).then((response) => {
        console.log('Подтяжка всех Юзеров компании -', response.data);
        context.commit('setAdvUsers', response.data);
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    /* Удаление юзера у advUser */
    async deleteAdvUserAction(context, id) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/user`;
      await axios.delete(
        url,
        {
          params: {
            id,
          },
          withCredentials: true,
        },
      ).then((response) => {
        // console.log('DELETE-ADV-USER-ACTION - THEN', response.data);
        context.commit('setAdvertiser', response.data);
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    /* Добавление advUser */
    async addAdvUserAction(context, user) {
      const payload = {
        email: user.email,
        role: user.role,
        fio: user.fio,
        scopes: user.scopes,
        phone: user.phone,
      };
      const url = `${process.env.VUE_APP_BASE_URL}/adv/user`;
      await axios.post(
        url,
        payload,
        {
          headers: {
            'Content-Type': 'application/json',
          },
          withCredentials: true,
        },
      ).then((response) => {
        console.log('Добавление - addAdvUserAction - ACTION - STORE', response.data);
        context.commit('setAdvertiser', response.data);
        // console.log('COMMIT', this.setAdvUser);
      }).catch((error) => {
        console.log(error);
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    async updateAdvertiserAction({ commit }, user) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/user/id/?id=${user.id}`;
      await axios.put(
        url,
        user,
        {
          headers: {
            'Access-Control-Allow-Origin': 'http://localhost:8080',
            'Content-Type': 'application/json',
          },
          withCredentials: true,
        },
      ).then((response) => {
        // console.log('Изменение - updateAdvertiserAction - ACTION - STORE', response.data);
        commit('setAdvertiser', response.data);
        Vue.$vToastify.success('Successfully');
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    // async getAdvUsersAction(context) {
    //   const url = `${process.env.VUE_APP_BASE_URL}/adv`;
    //   await axios.get(
    //     url,
    //     {
    //       withCredentials: true,
    //     },
    //   ).then((response) => {
    //     context.commit('setAdvUsers', response.data);
    //   }).catch((error) => {
    //     Vue.$vToastify.error({
    //       body: error,
    //     });
    //   });
    // },
    async getAdvUserAction(context) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/user`;
      await axios.get(
        url,
        {
          withCredentials: true,
        },
      ).then((response) => {
        context.commit('setAdvUser', response.data);
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    async updateAdvUserAction(context, userId) {
      const url = `${process.env.VUE_APP_BASE_URL}/adv/user/${userId}`;
      await axios.put(
        url,
        userId,
        {
          headers: {
            'Content-Type': 'application/json',
          },
          withCredentials: true,
        },
      ).then((response) => {
        // console.log('async updateAdvUserAction(context, data)', response.data);
        context.commit('setAdvUser', response.data);
        Vue.$vToastify.success('Successfully updated');
      }).catch((error) => {
        Vue.$vToastify.error({
          body: error,
        });
      });
    },
    // async updateAdvUserWithIdAction(context, [id, data]) {
    //   const url = `${process.env.VUE_APP_BASE_URL}/adv/user/id`;
    //   await axios.put(
    //     url,
    //     data,
    //     {
    //       params: {
    //         id,
    //       },
    //       withCredentials: true,
    //     },
    //   ).then((response) => {
    //     context.commit('setAdvertiser', response.data);
    //   }).catch((error) => {
    //     Vue.$vToastify.error({
    //       body: error,
    //     });
    //   });
    // },

    // setAdvertiserAction(context, data) {
    //   context.commit('setAdvertiser', data);
    // },
  },
  mutations: {
    setCompany(state, company) {
      state.company = company;
    },
    setAdvertiser(state, advertiser) {
      state.advertiser = advertiser;
    },

    setAdvUser(state, advUser) {
      console.log('MUTATIONS-USER -', advUser);
      state.advUser = advUser;
    },
    setAdvUsers(state, users) {
      state.advUsers = users;
    },
  },
  getters: {
    getCompany(state) {
      return state.company;
    },
    getAdvertiser(state) {
      // console.log('1 - getAdvertiser-STORE-GETTER', state.advertiser);
      return state.advertiser;
    },
    getAdvUser(state) {
      // console.log('2 - getAdvUser-STORE-GETTER', state.advUser);
      return state.advUser;
    },
    getAdvertiserUsers(state) {
      // console.log('3 - getAdvertiserUsers-STORE-GETTER', state.advertiser.users);
      return state.advUsers;
    },
  },
  modules: {},
};
