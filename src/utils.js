import {
  AWARDS_NAME,
  AWARDS_RUS_NAME,
  CARDS_STATUS,
  FORM_PAGES,
  INITIAL_SELECT_ANIMALS,
  INITIAL_SELECT_ANSWER_RESULT,
  INITIAL_SELECT_AUTO,
  INITIAL_SELECT_AWARD,
  INITIAL_SELECT_BUSINESS,
  INITIAL_SELECT_COUNTRY_HOUSE,
  INITIAL_SELECT_EDUCATION,
  INITIAL_SELECT_INCOME,
  INITIAL_SELECT_MARTIAL_STATUS,
  INITIAL_SELECT_QUESTION,
  INITIAL_SELECT_RELIGION,
  ROLES,
  RUS_ROLES,
  TIMES,
} from '@/const';

export const adaptRolesToSever = (role) => {
  switch (role) {
    case (RUS_ROLES.ADVERTISER): {
      return ROLES.DIRECTOR;
    }
    case (RUS_ROLES.AGENT): {
      return ROLES.MODERATOR;
    }
    default: {
      return ROLES.DIRECTOR;
    }
  }
};

export const adaptRolesToClient = (role) => {
  switch (role) {
    case (ROLES.DIRECTOR): {
      return RUS_ROLES.ADVERTISER;
    }
    case (ROLES.MODERATOR): {
      return RUS_ROLES.AGENT;
    }
    default: {
      return RUS_ROLES.ADVERTISER;
    }
  }
};


export const adaptCompanyInfoToClient = (data) => ({
  inn: data.inn,
  requisites: {
    bankAccount: data.requisites ? data.requisites.bank_account : '',
    bank: data.requisites ? data.requisites.bank : '',
    bankBik: data.requisites ? data.requisites.bank_bik : '',
    bankInn: data.requisites ? data.requisites.bank_inn : '',
    correspondentCheck: data.requisites ? data.requisites.correspondent_check : '',
    bankSubdivision: data.requisites ? data.requisites.bank_subdivision : '',
    bankSubdivisionAddress: data.requisites ? data.requisites.bank_subdivision_address : '',
  },
  user: {
    email: data.user ? data.user.email : '',
    role: data.user ? adaptRolesToClient(data.user.role) : '',
    fio: {
      surname: '',
      name: '',
      patronymic: '',
    },
  },
  kpp: data.kpp ? data.kpp : '',
  management: {
    name: data.management ? data.management.name : '',
    post: data.management ? data.management.post : '',
  },
  ogrn: data.ogrn ? data.ogrn : '',
  ogrnDate: data.ogrn_date ? new Date(data.ogrn_date).toISOString() : '',
  orgType: data.org_type ? data.org_type : '',
  name: {
    full: data.name.full ? data.name.full : '',
    fullWithOpf: data.name.full_with_opf ? data.name.full_with_opf : '',
    short: data.name.short ? data.name.short : '',
    shortWithOpf: data.name.short_with_opf ? data.name.short_with_opf : '',
  },
  fio: data.fio ? data.fio : '',
  address: {
    unrestrictedValue: data.address.unrestricted_value ? data.address.unrestricted_value : '',
    value: data.address.value ? data.address.value : '',
  },
  state: {
    actualityDate: data.state.actuality_date ? new Date(data.state.actuality_date).toISOString() : '',
    code: data.state.code ? data.state.code : '',
    liquidationDate: data.liquidation_date ? new Date(data.state.liquidation_date).toISOString() : '',
    registrationDate: data.state.registration_date ? new Date(data.state.registration_date).toISOString() : '',
    status: data.state.status ? data.state.status : '',
  },
});

export const getAges = (minValue, maxValue) => {
  const ages = [];

  for (let i = minValue; i <= maxValue; i += 1) {
    ages.push(i);
  }

  return ages;
};

export const getId = () => Math.ceil((Math.random() * (100 - 1) + 1) * 10000000);

export const getPagination = (data, maxActiveCards) => {
  const total = Math.ceil(data.length / maxActiveCards) + 1;
  const pagination = [];

  for (let i = 1; i < total; i += 1) {
    pagination.push(i);
  }

  return pagination;
};

export const getCurrentCards = (paginationValue, count, cards) => {
  const start = (paginationValue * count) - count;
  return cards.slice().splice(start, count);
};

export const getZalupa = (value) => {
  const questions = [];
  for (let i = 1; i <= value; i += 1) {
    questions.push({
      question: '',
      answers: [{
        text: '',
        answer: null,
      },
      {
        text: '',
        answer: null,
      },
      {
        text: '',
        answer: null,
      }],
    });
  }
  return questions;
};

export const getQuestions = (value) => {
  const questions = [];

  for (let i = 1; i <= value; i += 1) {
    questions.push({
      id: getId(),
      question: 'Какой сегодня день',
      answer1: {
        value: 'Понедельник',
        status: true,
      },
      answer2: {
        value: 'Среда',
        status: false,
      },
      answer3: {
        value: 'Пятница',
        status: false,
      },
    });
  }

  return questions;
};

export const transformAwards = (awards) => {
  /* Берем объект с наградами, преобразуем в массив с массивами [Ключ, значение],
  после фильтруем на наличие заполненных и преобразуем обратно в объект уже с оставшимися значениями */
  let transformedAwards = Object.entries(awards).filter((item) => item[1].answer_result
    && item[1].award_value
    && item[1].answer_result !== INITIAL_SELECT_ANSWER_RESULT
    && item[1].award_value !== INITIAL_SELECT_AWARD);

  transformedAwards = Object.fromEntries(transformedAwards.map(([key, value]) => [key, value]));

  return transformedAwards;
};

export const checkAwards = (awards) => {
  let isValid = false;
  const error = 'Награды';
  const transformedAwards = Object.entries(awards).filter((item) => item[1].answer_result
    && item[1].award_value
    && item[1].answer_result !== INITIAL_SELECT_ANSWER_RESULT
    && item[1].award_value !== INITIAL_SELECT_AWARD);

  if (transformedAwards.length) {
    isValid = true;
  }

  return { isValid, error };
};

export const adaptQuestionsToTemplate = (questions) => questions.slice().filter((item) => item.question && item.answer1.value && item.answer2.value && item.answer3.value && (item.answer1.status || item.answer2.status || item.answer3.status));

export const checkQuestions = (questions) => {
  const errorQuestions = 'Вопросы';
  let isValidQuestions = false;
  const transformedQuestions = adaptQuestionsToTemplate(questions);

  if (transformedQuestions.length) {
    isValidQuestions = true;
  }

  return { errorQuestions, isValidQuestions, transformedQuestions };
};

export const checkForm = (data) => {
  // console.log('CHECK_FORM_CONSOLE');
  const errors = [];
  const { errorQuestions, isValidQuestions } = checkQuestions(data.questions.value);
  const copiedData = { ...data };

  if (isValidQuestions === false) {
    errors.push(errorQuestions);
  }

  if (data.formName === FORM_PAGES.FUNNEL) {
    const { isValid, error } = checkAwards(data.awards.values);
    if (!isValid) {
      errors.push(error);
    }
  }

  // eslint-disable-next-line no-restricted-syntax
  for (const key in copiedData) {
    if (Array.isArray(data[key].value) && data[key].value.length === 0) {
      if (data[key].name !== errorQuestions) {
        errors.push(data[key].name);
      }
    } else if (data[key].value === null
      || data[key].value === ''
      || data[key].value === INITIAL_SELECT_QUESTION
      || data[key].value === INITIAL_SELECT_EDUCATION
      || data[key].value === INITIAL_SELECT_INCOME
      || data[key].value === INITIAL_SELECT_MARTIAL_STATUS
      || data[key].value === INITIAL_SELECT_ANIMALS
      || data[key].value === INITIAL_SELECT_AUTO
      || data[key].value === INITIAL_SELECT_COUNTRY_HOUSE
      || data[key].value === INITIAL_SELECT_BUSINESS
      || data[key].value === INITIAL_SELECT_RELIGION) {
      errors.push(data[key].name);
    }
  }

  return errors;
};

// data.questions.forEach((item) => {
//   if (item.question === '') {
//     errorMessage = 'Заполните вопросы';
//     errors.push(errorMessage);
//   }
//   item.answers.forEach((q) => {
//     if (q.text === '' && !q.answer) {
//       errorMessage = 'Подтвердите ответ';
//       errors.push(errorMessage);
//     }
//   });
// });

// export const checkFormFunnel = (data) => {
//   const errors = [];
//   let errorMessage = '';
//   // eslint-disable-next-line no-unused-vars
//   let i = 0;
//
//   data.awards.forEach((award) => {
//     if (['DISCOUNT', 'CASHBACK'].indexOf(award.award_type) >= 0) {
//       if (typeof (award.award_value) !== 'number') {
//         errorMessage = `Заполните размер скидки в блоке - ${award.award_type}`;
//         errors.push(errorMessage);
//       }
//       if (typeof (award.answer_result) !== 'number') {
//         errorMessage = `Заполните за какое количество ответов в блоке - ${award.award_type}`;
//         errors.push(errorMessage);
//       }
//     }
//     if (['CERTIFICATE', 'PROMOCODE', 'PRIZE', 'BLACKBOX', 'STOCK'].indexOf(award.award_type) >= 0) {
//       if (typeof (award.award_value) !== 'string') {
//         errorMessage = `Заполните наименование в блоке - ${award.award_type}`;
//         errors.push(errorMessage);
//       }
//       if (typeof (award.answer_result) !== 'number') {
//         errorMessage = `Заполните за какое количество ответов в блоке - ${award.award_type}`;
//         errors.push(errorMessage);
//       }
//     }
//     i += 1;
//   });
//
//   // data.errors = errors;
//   return errors;
// };

export function checkFormFunnel(data) {
  const result = [];
  // const errors = [];
  const errorMessage = 'Выберите хотя бы одну награду';
  const errorMessageAnswer = 'Заполните количество ответов';
  const errorMessageAward = 'Заполните награду';
  // let errorMessageQuestion = '';

  data.awards.forEach((award) => {
    // console.log(award.award_type, award.award_value, typeof (award.award_value), award.answer_result, typeof (award.answer_result));
    let error = {};
    if (['DISCOUNT', 'CASHBACK'].indexOf(award.award_type) >= 0) {
      if (typeof (award.award_value) === 'number') {
        error.awardType = award.award_type;
        error.awardValue = true;
      } else {
        error.awardType = award.award_type;
        error.awardValue = false;
      }

      if (typeof (award.answer_result) === 'number') {
        error.awardType = award.award_type;
        error.answerResult = true;
      } else {
        // eslint-disable-next-line no-unused-expressions
        error.awardType = award.award_type;
        error.answerResult = false;
      }
    }

    if (['CERTIFICATE', 'PROMOCODE', 'PRIZE', 'BLACKBOX', 'STOCK'].indexOf(award.award_type) >= 0) {
      if (award.award_value.length > 0) {
        error.awardType = award.award_type;
        error.awardValue = true;
      } else {
        error.awardType = award.award_type;
        error.awardValue = false;
      }

      if (typeof (award.answer_result) === 'number') {
        error.awardType = award.award_type;
        error.answerResult = true;
      } else {
        error.awardType = award.award_type;
        error.answerResult = false;
      }
    }

    if (error !== {}) {
      result.push(error);
      error = {};
    }
  });

  const errors = [];
  let errorMessageQuestion = '';
  let awardsMissing = true;
  data.questions.forEach((item) => {
    console.log('ITEM', item);
    if (item.question === '') {
      errorMessageQuestion = 'Заполните вопрос';
    } else {
      errorMessageQuestion = '';
    }
    item.answers.forEach((q) => {
      if (q.text === '' && q.answer === false) {
        errorMessageQuestion = 'Подтвердите ответ';
      } else {
        errorMessageQuestion = '';
      }
    });
  });
  if (errorMessageQuestion !== '') {
    errors.push(errorMessageQuestion);
  }

  // eslint-disable-next-line no-restricted-syntax
  for (const i in result) {
    if (result[i].awardValue === true || result[i].answerResult === true) {
      awardsMissing = false;
    }
  }

  if (awardsMissing === true) {
    errors.push(errorMessage);
  } else {
    // eslint-disable-next-line no-restricted-syntax
    for (const i in result) {
      if (result[i].awardValue !== result[i].answerResult) {
        if (!result[i].awardValue) {
          errors.push(`${result[i].awardType} - ${errorMessageAward}`);
        }
        if (!result[i].answerResult) {
          errors.push(`${result[i].awardType} - ${errorMessageAnswer}`);
        }
      }
    }
  }

  // eslint-disable-next-line no-param-reassign
  data.result = result;
  // eslint-disable-next-line no-param-reassign
  data.errors = errors;
  // console.log(errors);
  return errors;
}

// eslint-disable-next-line no-undef
// const newFan = checkFormFunnel(data);
// console.log(newFan);

// if (data.name === FORM_PAGES.FUNNEL) {
//   // const { isValid, error } = checkAwards(data.awards);
//   const { isValid, error } = checkQuestion(data.questions);
//   if (!isValid) {
//     console.log('checkQuestion', checkQuestion);
//     errors.push(error);
//   }
// }
// const checkQuestion = checkQuestionsFunnel(data.questions);
// // const { isValid, error } = checkQuestion(data.questions);
// console.log('checkQuestion', checkQuestion);
// if (data.name === FORM_PAGES.FUNNEL) {
//   if (data.questions.length || data.answers.length || data.length === 0) {
//     errors.push(checkQuestion);
//   }
// }
// if (data.name === FORM_PAGES.FUNNEL) {
//   // const { isValid, error } = checkAwards(data.awards);
//   const { isValid, error } = checkQuestion(data.questions);
//   if (!isValid) {
//     console.log('checkQuestion', checkQuestion);
//     errors.push(error);
//   }
// }
// if (Array.isArray(data) && data.length === 0) {
//   //     if (data.name !== errorQuestions) {
//   //       errors.push(data.name);
//   //     }
//   //   } else if (data === null
//   //     || data === ''
//   //     || data === INITIAL_SELECT_QUESTION) {
//   //     errors.push(data.name);
// }
// if (isValidQuestions === false) {
//   errors.push(errorQuestions);
// }
// if (data.name === FORM_PAGES.FUNNEL) {
//   // const { isValid, error } = checkAwards(data.awards);
//   const { isValid, error } = checkQuestion(data.questions);
//   if (!isValid) {
//     console.log('checkQuestion', checkQuestion);
//     errors.push(error);
//   }
// }
// const copiedData = { ...data };
// console.log('CHECK_FORM_CONSOLE', data);
// //
// //
// if (data.name === FORM_PAGES.FUNNEL) {
//   const { isValid, error } = checkAwards(data.awards);
//   if (!isValid) {
//     errors.push(error);
//   }
// }
// // eslint-disable-next-line no-restricted-syntax,guard-for-in
// for (const key in copiedData) {
//   console.log('KEY -', key);
//   console.log('copiedData -', copiedData);
//   if (Array.isArray(data) && data.length === 0) {
//     if (data.name !== errorQuestions) {
//       errors.push(data.name);
//     }
//   } else if (data === null
//     || data === ''
//     || data === INITIAL_SELECT_QUESTION) {
//     errors.push(data.name);
//   }
// }

export const humanizeTimes = (time) => {
  switch (time) {
    case (TIMES.MORNING.value): {
      return TIMES.MORNING.name;
    }
    case (TIMES.DAY.value): {
      return TIMES.DAY.name;
    }
    case (TIMES.EVENING.value): {
      return TIMES.EVENING.name;
    }
    default: {
      return TIMES.NIGHT.name;
    }
  }
};

export const lookTheme = (theme, themes) => themes.some((item) => item.value === theme);

export const getTheme = (theme, themes) => {
  const index = themes.findIndex((item) => item.value === theme);
  return themes[index].name;
};

export const adaptTimesToString = (times) => times.map((time) => humanizeTimes(time)).join(', ');

export const humanizeAwardsName = (name) => {
  switch (name) {
    case (AWARDS_NAME.DISCOUNT): {
      return AWARDS_RUS_NAME.DISCOUNT;
    }
    case (AWARDS_NAME.CERTIFICATE): {
      return AWARDS_RUS_NAME.CERTIFICATE;
    }
    case (AWARDS_NAME.CASHBACK): {
      return AWARDS_RUS_NAME.CASHBACK;
    }
    case (AWARDS_NAME.PROMO_CODE): {
      return AWARDS_RUS_NAME.PROMO_CODE;
    }
    case (AWARDS_NAME.PRIZE): {
      return AWARDS_RUS_NAME.PRIZE;
    }
    case (AWARDS_NAME.BLACK_BOX): {
      return AWARDS_RUS_NAME.BLACK_BOX;
    }
    case (AWARDS_NAME.STOCK): {
      return AWARDS_RUS_NAME.STOCK;
    }
    default: {
      return 'Неизвестная награда';
    }
  }
};

export const adaptAwardsToTemplate = (award) => Object.entries(award).slice().map(([key, value]) => ({ key, name: humanizeAwardsName(key), value }));

export const getTimesWeight = (time) => {
  switch (time) {
    case (TIMES.MORNING.value): {
      return 4;
    }
    case (TIMES.DAY.value): {
      return 3;
    }
    case (TIMES.EVENING.value): {
      return 2;
    }
    case (TIMES.NIGHT.value): {
      return 1;
    }
    default: {
      return 0;
    }
  }
};

export const getSortingTimes = (times) => times.sort((a, b) => getTimesWeight(b) - getTimesWeight(a));

export const humanizeTown = (town, allTowns) => {
  const index = allTowns.findIndex((item) => item.value === town);
  return allTowns[index].name;
};

export const getInitialFunnelForm = () => ({
  name: FORM_PAGES.FUNNEL,
  id: `${getId()}-${new Date().toLocaleDateString()}`,
  // id: String,
  href: '',
  question_count: 1,
  contract: '',
  questions: [
    {
      question: '',
      // question: getQuestions(1),
      answers: [
        {
          text: '',
          answer: false,
        },
        {
          text: '',
          answer: false,
        },
        {
          text: '',
          answer: false,
        },
      ],
    }],
  awards: [
    // name: 'Награды',
    {
      answer_result: INITIAL_SELECT_ANSWER_RESULT,
      award_value: INITIAL_SELECT_AWARD,
      award_type: 'DISCOUNT',
    },
    {
      answer_result: INITIAL_SELECT_ANSWER_RESULT,
      award_value: '',
      award_type: 'CERTIFICATE',
    },
    {
      answer_result: INITIAL_SELECT_ANSWER_RESULT,
      award_value: INITIAL_SELECT_AWARD,
      award_type: 'CASHBACK',
    },
    {
      answer_result: INITIAL_SELECT_ANSWER_RESULT,
      award_value: '',
      award_type: 'PROMOCODE',
    },
    {
      answer_result: INITIAL_SELECT_ANSWER_RESULT,
      award_value: '',
      award_type: 'PRIZE',
    },
    {
      answer_result: INITIAL_SELECT_ANSWER_RESULT,
      award_value: '',
      award_type: 'BLACKBOX',
    },
    {
      answer_result: INITIAL_SELECT_ANSWER_RESULT,
      award_value: '',
      award_type: 'STOCK',
    },
  ],
  expired: Number,
  points_for_receiving_awards: [],
});

export const getInitialTranslatorForm = () => ({
  formName: FORM_PAGES.TRANSLATOR,
  id: `${getId()}-${new Date().toLocaleDateString()}`,
  status: CARDS_STATUS.NEW,
  theme: {
    name: 'Тема контента',
    value: [],
  },
  currentFrame: {
    frameIndex: 1,
  },
  technicalSupport: {
    name: 'Техническая поддержка',
    value: 30,
  },
  views: {
    name: 'Количество просмотров',
    value: 1000,
  },
  file: {
    name: 'Загрузить файл',
    value: null,
  },
  questions: {
    name: 'Вопрос',
    value: getQuestions(1),
  },
  categories: {
    name: 'Категория контента',
    value: '',
  },
  gender: {
    name: 'Пол',
    value: [],
  },
  ages: {
    name: 'Возраст',
    value: {
      from: 18,
      to: 18,
    },
  },
  education: {
    name: INITIAL_SELECT_EDUCATION,
    value: INITIAL_SELECT_EDUCATION,
  },
  income: {
    name: INITIAL_SELECT_INCOME,
    value: INITIAL_SELECT_INCOME,
  },
  martialStatus: {
    name: INITIAL_SELECT_MARTIAL_STATUS,
    value: INITIAL_SELECT_MARTIAL_STATUS,
  },
  religion: {
    name: INITIAL_SELECT_RELIGION,
    value: INITIAL_SELECT_RELIGION,
  },
  animals: {
    name: INITIAL_SELECT_ANIMALS,
    value: INITIAL_SELECT_ANIMALS,
  },
  auto: {
    name: INITIAL_SELECT_AUTO,
    value: INITIAL_SELECT_AUTO,
  },
  countryHouse: {
    name: INITIAL_SELECT_COUNTRY_HOUSE,
    value: INITIAL_SELECT_COUNTRY_HOUSE,
  },
  business: {
    name: INITIAL_SELECT_BUSINESS,
    value: INITIAL_SELECT_BUSINESS,
  },
  geolocationName: {
    name: 'Геолокация',
    value: [],
  },
  timeOfTheDay: {
    name: 'Время показа',
    value: [],
  },
  duration: {
    name: 'Время ролика',
    value: null,
  },
  price: {
    name: 'Сумма за публикацию',
    value: 100,
  },
});

export const getDefaultUser = () => ({
  id: getId(),
  fio: '',
  email: '',
  phone: '',
  role: 'director',
  scopes: [],
});

export const getFormattedDate = (date) => {
  const [year, month, day] = date.split('-');
  return `${day}.${month}.${year}`;
};
