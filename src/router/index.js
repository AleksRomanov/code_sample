import Vue from 'vue';
import VueRouter from 'vue-router';
// import UserProfile from '../views/user-profile.vue';
import store from '@/store/index';
import MainPage from '../views/main-page.vue';
import Analytics from '../views/personal-area/analytics.vue';
import MainLayout from '../views/main-layout.vue';
import Auth from '../views/auth/auth.vue';
import Login from '../views/auth/login.vue';
import Signup1 from '../views/auth/adv-signup-steps/signup-step1.vue';
import Signup2 from '../views/auth/adv-signup-steps/signup-step2.vue';
import Signup3 from '../views/auth/adv-signup-steps/signup-step3.vue';
import AdvInfo from '../views/adv/adv-info.vue';
import AdvAbout from '../views/adv/adv-about.vue';
import AdvAboutInfo from '../views/adv/adv-about-info.vue';
import AdvAboutInfoEdit from '../views/adv/adv-about-info-edit.vue';
import AdvUsers from '../views/adv/adv-users.vue';
import AdvUser from '../views/adv/adv-user.vue';
import AdvUserEdit from '../views/adv/adv-user-edit.vue';
import AdvUserInfo from '../views/adv/adv-user-info.vue';
import DashboardLayout from '@/views/personal-area/dashboard-layout.vue';
import FunnelLayout from '@/views/personal-area/funnel/funnel-layout.vue';
import Notifications from '@/views/personal-area/notifications.vue';
import History from '@/views/personal-area/history.vue';
import TranslatorLayout from '@/views/personal-area/translator/translator-layout.vue';
import Terms from '@/views/terms.vue';
import Rules from '@/views/rules.vue';
import HelpLayout from '@/views/help/help-layout.vue';
import { ROUTES } from '@/router/routes-config';
import FunnelCreate from '@/views/personal-area/funnel/funnel-create.vue';
import FunnelActive from '@/views/personal-area/funnel/funnel-active.vue';
import FunnelArchive from '@/views/personal-area/funnel/funnel-archive.vue';
import FunnelHistory from '@/views/personal-area/funnel/funnel-history.vue';
import FunnelShow from '@/views/personal-area/funnel/funnel-show.vue';
import TranslatorCreate from '@/views/personal-area/translator/translator-create.vue';
import TranslatorActive from '@/views/personal-area/translator/translator-active.vue';
import TranslatorArchive from '@/views/personal-area/translator/translator-archive.vue';
import TranslatorHistory from '@/views/personal-area/translator/translator-history.vue';
import TranslatorShow from '@/views/personal-area/translator/translator-show.vue';
import FunnelDrafts from '@/views/personal-area/funnel/funnel-drafts.vue';
import FunnelPending from '@/views/personal-area/funnel/funnel-pending.vue';
import TranslatorDrafts from '@/views/personal-area/translator/translator-drafts.vue';
import TranslatorPending from '@/views/personal-area/translator/translator-pending.vue';
import NotFound from '@/views/not-found.vue';
import Companies from '@/views/personal-area/companies.vue';
import Recovery from '@/views/auth/recovery.vue';
import { ADV_USER_SCOPES } from '@/const';

Vue.use(VueRouter);

const routes = [
  {
    path: ROUTES.MAIN_LAYOUT.ROUTE,
    name: ROUTES.MAIN_LAYOUT.NAME,
    component: MainLayout,
    children: [
      {
        path: ROUTES.MAIN_PAGE.ROUTE,
        name: ROUTES.MAIN_PAGE.NAME,
        component: MainPage,
        meta: {
          title: 'Project!',
        },
      },
      {
        path: ROUTES.DASHBOARD_LAYOUT.ROUTE,
        name: ROUTES.DASHBOARD_LAYOUT.NAME,
        component: DashboardLayout,
        children: [
          {
            path: ROUTES.ANALYTICS.ROUTE,
            name: ROUTES.ANALYTICS.NAME,
            component: Analytics,
            meta: {
              title: 'Аналитика',
              accessRights: [
                ADV_USER_SCOPES.FINANCE.VALUE,
                ADV_USER_SCOPES.STATISTICS.VALUE,
                ADV_USER_SCOPES.TOOLS.VALUE,
                ADV_USER_SCOPES.FINANCE.VALUE,
              ],
            },
          },
          {
            path: ROUTES.FUNNEL_LAYOUT.ROUTE,
            name: ROUTES.FUNNEL_LAYOUT.NAME,
            component: FunnelLayout,
            redirect: {
              name: ROUTES.FUNNEL_CREATE.NAME,
            },
            meta: {
              title: 'Воронка',
              accessRights: [
                ADV_USER_SCOPES.TOOLS.VALUE,
                ADV_USER_SCOPES.FINANCE.VALUE,
              ],
            },
            children: [
              {
                path: ROUTES.FUNNEL_CREATE.ROUTE,
                name: ROUTES.FUNNEL_CREATE.NAME,
                component: FunnelCreate,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS.VALUE,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.FUNNEL_DRAFTS.ROUTE,
                name: ROUTES.FUNNEL_DRAFTS.NAME,
                component: FunnelDrafts,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS.VALUE,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.FUNNEL_PENDING.ROUTE,
                name: ROUTES.FUNNEL_PENDING.NAME,
                component: FunnelPending,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS.VALUE,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.FUNNEL_ACTIVE.ROUTE,
                name: ROUTES.FUNNEL_ACTIVE.NAME,
                component: FunnelActive,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.FUNNEL_ARCHIVE.ROUTE,
                name: ROUTES.FUNNEL_ARCHIVE.NAME,
                component: FunnelArchive,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.FUNNEL_HISTORY.ROUTE,
                name: ROUTES.FUNNEL_HISTORY.NAME,
                component: FunnelHistory,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.FUNNEL_SHOW.ROUTE,
                name: ROUTES.FUNNEL_SHOW.NAME,
                component: FunnelShow,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
            ],
          },
          {
            path: ROUTES.TRANSLATOR_LAYOUT.ROUTE,
            name: ROUTES.TRANSLATOR_LAYOUT.NAME,
            component: TranslatorLayout,
            redirect: {
              name: ROUTES.TRANSLATOR_CREATE.NAME,
            },
            meta: {
              title: 'Транслятор',
              accessRights: [
                ADV_USER_SCOPES.FINANCE.VALUE,
                ADV_USER_SCOPES.TOOLS,
                ADV_USER_SCOPES.STATISTICS.VALUE,
              ],
            },
            children: [
              {
                path: ROUTES.TRANSLATOR_CREATE.ROUTE,
                name: ROUTES.TRANSLATOR_CREATE.NAME,
                component: TranslatorCreate,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS.VALUE,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.TRANSLATOR_DRAFTS.ROUTE,
                name: ROUTES.TRANSLATOR_DRAFTS.NAME,
                component: TranslatorDrafts,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.TRANSLATOR_PENDING.ROUTE,
                name: ROUTES.TRANSLATOR_PENDING.NAME,
                component: TranslatorPending,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.TRANSLATOR_ACTIVE.ROUTE,
                name: ROUTES.TRANSLATOR_ACTIVE.NAME,
                component: TranslatorActive,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.TRANSLATOR_ARCHIVE.ROUTE,
                name: ROUTES.TRANSLATOR_ARCHIVE.NAME,
                component: TranslatorArchive,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.TRANSLATOR_HISTORY.ROUTE,
                name: ROUTES.TRANSLATOR_HISTORY.NAME,
                component: TranslatorHistory,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
              {
                path: ROUTES.TRANSLATOR_SHOW.ROUTE,
                name: ROUTES.TRANSLATOR_SHOW.NAME,
                component: TranslatorShow,
                meta: {
                  accessRights: [
                    ADV_USER_SCOPES.FINANCE.VALUE,
                    ADV_USER_SCOPES.TOOLS,
                    ADV_USER_SCOPES.STATISTICS.VALUE,
                  ],
                },
              },
            ],
          },
          {
            path: ROUTES.NOTIFICATIONS.ROUTE,
            name: ROUTES.NOTIFICATIONS.NAME,
            component: Notifications,
          },
          {
            path: ROUTES.COMPANIES.ROUTE,
            name: ROUTES.COMPANIES.NAME,
            component: Companies,
            meta: {
              accessRights: [
                ADV_USER_SCOPES.FINANCE.VALUE,
                ADV_USER_SCOPES.TOOLS,
                ADV_USER_SCOPES.STATISTICS.VALUE,
              ],
            },
          },
          {
            path: ROUTES.HISTORY.ROUTE,
            name: ROUTES.HISTORY.NAME,
            component: History,
          },
          {
            path: ROUTES.ADV_INFO.ROUTE,
            name: ROUTES.ADV_INFO.NAME,
            component: AdvInfo,
            children: [
              {
                path: ROUTES.ADV_ABOUT.ROUTE,
                name: ROUTES.ADV_ABOUT.NAME,
                component: AdvAbout,
                children: [
                  {
                    path: ROUTES.ADV_ABOUT_INFO.ROUTE,
                    name: ROUTES.ADV_ABOUT_INFO.NAME,
                    component: AdvAboutInfo,
                  },
                  {
                    path: ROUTES.ADV_ABOUT_INFO_EDIT.ROUTE,
                    name: ROUTES.ADV_ABOUT_INFO_EDIT.NAME,
                    component: AdvAboutInfoEdit,
                    meta: {
                      accessRights: [
                        ADV_USER_SCOPES.FINANCE.VALUE,
                        ADV_USER_SCOPES.STATISTICS.VALUE,
                        ADV_USER_SCOPES.TOOLS.VALUE,
                      ],
                    },
                  },
                ],
              },
              {
                path: ROUTES.ADV_USERS.ROUTE,
                name: ROUTES.ADV_USERS.NAME,
                component: AdvUsers,
              },
            ],
          },
          {
            path: ROUTES.HELP.ROUTE,
            name: ROUTES.HELP.NAME,
            component: HelpLayout,
          },
          {
            path: ROUTES.TERMS.ROUTE,
            name: ROUTES.TERMS.NAME,
            component: Terms,
          },
          {
            path: ROUTES.RULES.ROUTE,
            name: ROUTES.RULES.NAME,
            component: Rules,
          },
        ],
      },
      {
        path: ROUTES.ADV_USER.ROUTE,
        name: ROUTES.ADV_USER.NAME,
        component: AdvUser,
        children: [
          {
            path: ROUTES.ADV_USER_INFO.ROUTE,
            name: ROUTES.ADV_USER_INFO.NAME,
            component: AdvUserInfo,
          },
          {
            path: ROUTES.ADV_USER_EDIT.ROUTE,
            name: ROUTES.ADV_USER_EDIT.NAME,
            component: AdvUserEdit,
            meta: {
              accessRights: [
                ADV_USER_SCOPES.FINANCE.VALUE,
              ],
            },
          },
        ],
      },
    ],
  },
  {
    path: ROUTES.AUTH.ROUTE,
    name: ROUTES.AUTH.NAME,
    component: Auth,
    children: [
      {
        path: ROUTES.RECOVERY.ROUTE,
        name: ROUTES.RECOVERY.NAME,
        component: Recovery,
      },
      {
        path: ROUTES.LOGIN.ROUTE,
        name: ROUTES.LOGIN.NAME,
        component: Login,
      },
      {
        path: ROUTES.SIGN_UP_STEP_ONE.ROUTE,
        name: ROUTES.SIGN_UP_STEP_ONE.NAME,
        component: Signup1,
      },
      {
        path: ROUTES.SIGN_UP_STEP_TWO.ROUTE,
        name: ROUTES.SIGN_UP_STEP_TWO.NAME,
        component: Signup2,
      },
      {
        path: ROUTES.SIGN_UP_STEP_THREE.ROUTE,
        name: ROUTES.SIGN_UP_STEP_THREE.NAME,
        component: Signup3,
      },
    ],
  },
  {
    path: ROUTES.NOT_FOUND.ROUTE,
    name: ROUTES.NOT_FOUND.NAME,
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

const DEFAULT_TITLE = 'Project';

router.afterEach((to) => {
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE;
  });
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.accessRights)) {
    const { accessRights } = to.meta;
    const userScopes = store.getters.getUserScopes;
    if (Vue.prototype.$checkAccess(userScopes, accessRights)) {
      next();
      return;
    }
    next(ROUTES.MAIN_PAGE.ROUTE);
    return;
  }
  next();
});

export default router;
