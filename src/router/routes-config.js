// Сделано для гибкой настройки роутов в одном месте.
export const ROUTES = {
  MAIN_LAYOUT: {
    NAME: 'main-layout',
    ROUTE: '',
  },
  MAIN_PAGE: {
    NAME: 'main-page',
    ROUTE: '/',
  },
  DASHBOARD_LAYOUT: {
    NAME: 'dashboard-layout',
    ROUTE: '',
  },
  ANALYTICS: {
    NAME: 'analytics',
    ROUTE: '/analytics',
  },
  HISTORY: {
    NAME: 'history',
    ROUTE: '/history',
  },
  FUNNEL_LAYOUT: {
    NAME: 'funnel-layout',
    ROUTE: '/funnel',
  },
  FUNNEL_CREATE: {
    NAME: 'funnel-create',
    ROUTE: '/funnel/create',
  },
  FUNNEL_DRAFTS: {
    NAME: 'funnel-drafts',
    ROUTE: '/funnel/drafts',
  },
  FUNNEL_PENDING: {
    NAME: 'funnel-pending',
    ROUTE: '/funnel/pending',
  },
  FUNNEL_ACTIVE: {
    NAME: 'funnel-active',
    ROUTE: '/funnel/active',
  },
  FUNNEL_ARCHIVE: {
    NAME: 'funnel-archive',
    ROUTE: '/funnel/archive',
  },
  FUNNEL_HISTORY: {
    NAME: 'funnel-history',
    ROUTE: '/funnel/history',
  },
  FUNNEL_SHOW: {
    NAME: 'funnel-show',
    ROUTE: '/funnel/show',
  },
  TRANSLATOR_LAYOUT: {
    NAME: 'translator-layout',
    ROUTE: '/translator',
  },
  TRANSLATOR_CREATE: {
    NAME: 'translator-create',
    ROUTE: '/translator/create',
  },
  TRANSLATOR_DRAFTS: {
    NAME: 'translator-drafts',
    ROUTE: '/translator/drafts',
  },
  TRANSLATOR_PENDING: {
    NAME: 'translator-pending',
    ROUTE: '/translator/pending',
  },
  TRANSLATOR_ACTIVE: {
    NAME: 'translator-active',
    ROUTE: '/translator/active',
  },
  TRANSLATOR_ARCHIVE: {
    NAME: 'translator-archive',
    ROUTE: '/translator/archive',
  },
  TRANSLATOR_HISTORY: {
    NAME: 'translator-history',
    ROUTE: '/translator/history',
  },
  TRANSLATOR_SHOW: {
    NAME: 'translator-show',
    ROUTE: '/translator/show',
  },
  ADV_USER: {
    NAME: 'adv-user',
    ROUTE: '/adv/user',
  },
  ADV_USER_INFO: {
    NAME: 'adv-user-info',
    ROUTE: '',
  },
  ADV_USER_EDIT: {
    NAME: 'adv-user-edit',
    ROUTE: 'edit',
  },
  ADV_INFO: {
    NAME: 'adv-info',
    ROUTE: '/adv/about/info',
  },
  ADV_ABOUT: {
    NAME: 'adv-about',
    ROUTE: '/adv/about',
  },
  ADV_ABOUT_INFO: {
    NAME: 'adv-about-info',
    ROUTE: '/adv/about/info',
  },
  ADV_ABOUT_INFO_EDIT: {
    NAME: 'adv-about-info-edit',
    ROUTE: '/adv/about/edit',
  },
  ADV_USERS: {
    NAME: 'adv-users',
    ROUTE: '/adv/users',
  },
  QUIZ: {
    NAME: 'quiz',
    ROUTE: '/quiz',
  },
  QUIZ_CREATE: {
    NAME: 'quiz-create',
    ROUTE: '/quiz/create',
  },
  QUIZ_ACTIVE: {
    NAME: 'quiz-active',
    ROUTES: '/quiz/active',
  },
  QUIZ_ARCHIVE: {
    NAME: 'quiz-archive',
    ROUTE: '/quiz/archive',
  },
  QUIZ_HISTORY: {
    NAME: 'quiz-history',
    ROUTE: '/quiz/history',
  },
  QUIZ_SHOW: {
    NAME: 'quiz-show',
    ROUTE: '/quiz/show/:id',
  },
  QUIZ_EDIT: {
    NAME: 'quiz-edit',
    ROUTE: '/quiz/edit/:id',
  },
  ADVERTISING: {
    NAME: 'advertising',
    ROUTE: '/advertising',
  },
  ADVERTISING_CREATE: {
    NAME: 'advertising-create',
    ROUTE: '/advertising/create',
  },
  ADVERTISING_ACTIVE: {
    NAME: 'advertising-active',
    ROUTE: '/advertising/active',
  },
  ADVERTISING_ARCHIVE: {
    NAME: 'advertising-archive',
    ROUTE: '/advertising/archive',
  },
  ADVERTISING_HISTORY: {
    NAME: 'advertising-history',
    ROUTE: '/advertising/history',
  },
  ADVERTISING_EDIT: {
    NAME: 'advertising-edit',
    ROUTE: '/advertising/edit/:id',
  },
  ADVERTISING_SHOW: {
    NAME: 'advertising-show',
    ROUTE: '/advertising/show/:id',
  },
  NOTIFICATIONS: {
    NAME: 'notifications',
    ROUTE: '/notifications',
  },
  TERMS: {
    NAME: 'terms',
    ROUTE: '/terms',
  },
  RULES: {
    NAME: 'rules',
    ROUTE: '/rules',
  },
  HELP: {
    NAME: 'help',
    ROUTE: '/help',
  },
  USERS: {
    NAME: 'users',
    ROUTE: '/users',
  },
  TICKETS: {
    NAME: 'tickets',
    ROUTE: '/tickets',
  },
  AUTH: {
    NAME: 'auth',
    ROUTE: '/auth',
  },
  LOGIN: {
    NAME: 'login',
    ROUTE: '/login',
  },
  SIGN_UP_STEP_ONE: {
    NAME: 'signup-1',
    ROUTE: '/signup/1',
  },
  SIGN_UP_STEP_TWO: {
    NAME: 'signup-2',
    ROUTE: '/signup/2',
  },
  SIGN_UP_STEP_THREE: {
    NAME: 'signup-3',
    ROUTE: '/signup/3',
  },
  COMPANIES: {
    NAME: 'companies',
    ROUTE: '/companies',
  },
  RECOVERY: {
    NAME: 'recovery',
    ROUTE: '/recovery',
  },
  NOT_FOUND: {
    NAME: 'not-found',
    ROUTE: '*',
  },
};
