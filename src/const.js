// В этом файле хранятся все константы.
export const MAX_ACTIVE_CARDS = 3;
export const MAX_HISTORY_CARDS = 6;
export const MAX_QUESTIONS = 30;
export const INITIAL_PAGINATION_VALUE = 1;
export const INITIAL_SELECT_QUESTION = 'Количество вопросов';
export const INITIAL_SELECT_EDUCATION = 'Образование';
export const INITIAL_SELECT_INCOME = 'Доход';
export const INITIAL_SELECT_MARTIAL_STATUS = 'Семейное положение';
export const INITIAL_SELECT_RELIGION = 'Религия';
export const INITIAL_SELECT_AWARD = 'Выбрать';
export const INITIAL_SELECT_ANIMALS = 'Животные';
export const INITIAL_SELECT_AUTO = 'Авто';
export const INITIAL_SELECT_COUNTRY_HOUSE = 'Дача';
export const INITIAL_SELECT_BUSINESS = 'Бизнес';
export const INITIAL_SELECT_ANSWER_RESULT = 'Ответов';
export const INITIAL_SELECT_COMPANY_TYPE = 'Тип компании';
export const INITIAL_SELECT_TAXATION = 'Форма налогообложение';
export const CHART_DAYS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30];
export const CHART_TIMES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0];

export const DRAFT_TYPES = {
  FUNNEL: 'funnel',
  TRANSLATOR: 'translator',
};

export const TAXATION_TYPE = [
  {
    name: 'Общая',
    value: 'genera',
  },
  {
    name: 'Упрощенная',
    value: 'simplified',
  },
  {
    name: 'Патентная',
    value: 'patent',
  },
  {
    name: 'Единый сельскохозяйственный налог',
    value: 'agricultural',
  },
  {
    name: 'Единый налог на вмененный доход',
    value: 'imputed-income',
  },
];

export const COMPANY_TYPE = {
  AGENCY: {
    name: 'Агенство',
    value: 'AGENCY',
  },
  COMPANY: {
    name: 'Компания',
    value: 'COMPANY',
  },
};

export const CHART_WEEK_DAY = [
  {
    value: 1,
    name: 'Понедельник',
  },
  {
    value: 2,
    name: 'Вторник',
  },
  {
    value: 3,
    name: 'Среда',
  },
  {
    value: 4,
    name: 'Четверг',
  },
  {
    value: 5,
    name: 'Пятница',
  },
  {
    value: 6,
    name: 'Суббота',
  },
  {
    value: 0,
    name: 'Воскресенье',
  },
];

export const CHART_MONTH = [
  {
    value: 0,
    name: 'Январь',
  },
  {
    value: 1,
    name: 'Февраль',
  },
  {
    value: 2,
    name: 'Март',
  },
  {
    value: 3,
    name: 'Апрель',
  },
  {
    value: 4,
    name: 'Май',
  },
  {
    value: 5,
    name: 'Июнь',
  },
  {
    value: 6,
    name: 'Июль',
  },
  {
    value: 7,
    name: 'Август',
  },
  {
    value: 8,
    name: 'Сентябрь',
  },
  {
    value: 9,
    name: 'Октябрь',
  },
  {
    value: 10,
    name: 'Ноябрь',
  },
  {
    value: 11,
    name: 'Декабрь',
  },
];

export const TIME_PERIODS = {
  DAY: 'day',
  WEEK: 'week',
  MONTH: 'month',
  YEAR: 'year',
};

export const USER_MAP_STATUS = {
  ACTIVE: 'active',
  PERSPECTIVE: 'perspective',
  FAR: 'far',
};

export const CARDS_STATUS = {
  ACTIVE: 'active',
  NOT_PAID: 'not_paid',
  ARCHIVE: 'archive',
  NEW: 'new',
  DRAFT: 'draft',
  PENDING: 'pending',
};

export const FORM_PAGES = {
  FUNNEL: 'funnel',
  TRANSLATOR: 'translator',
};

export const CONTENT_TYPES = {
  VIDEO: 'video',
  BANNER: 'banner',
  AUDIO: 'audio',
};

export const ROLES = {
  DIRECTOR: 'DIRECTOR',
  MODERATOR: 'MODERATOR',
};

export const RUS_ROLES = {
  ADVERTISER: 'Рекламодатель',
  AGENT: 'Агент',
};

// Должность пользователя
export const ADV_USER_ROLES = [
  {
    LABEL: 'Директор',
    VALUE: 'director',
  },
  {
    LABEL: 'Бухгалтер',
    VALUE: 'accountant',
  },
];

// Права доступа пользователя
export const ADV_USER_SCOPES = {
  STATISTICS: {
    LABEL: 'Статистика',
    VALUE: 'STATISTICS',
  },
  FINANCE: {
    LABEL: 'Финансы',
    VALUE: 'FINANCES',
  },
  TOOLS: {
    LABEL: 'Работа с инструментами',
    VALUE: 'TOOLS',
  },
};

export const TIMES = {
  MORNING: {
    value: 'MORNING',
    name: 'Утро с 06 до 12 ч.',
  },
  DAY: {
    value: 'DAY',
    name: 'День с 12 до 18 ч.',
  },
  EVENING: {
    value: 'EVENING',
    name: 'Вечер с 18 до 00 ч',
  },
  NIGHT: {
    value: 'NIGHT',
    name: 'Ночь с 00 до 06 ч.',
  },
};

export const SHOW_TIME_CHECKBOXES = [
  TIMES.MORNING,
  TIMES.DAY,
  TIMES.EVENING,
  TIMES.NIGHT,
];

export const FORMS = [
  {
    id: 'main',
    name: 'Основное',
  },
  {
    id: 'question-answer',
    name: 'Вопрос -ответ',
  },
  {
    id: 'auditory',
    name: 'Целевая аудитория',
  },
  {
    id: 'geolocation',
    name: 'Геолокация',
  },
  {
    id: 'show-time',
    name: 'Время показа',
  },
  {
    id: 'price',
    name: 'Стоимость',
  },
  {
    id: 'awards',
    name: 'Награды',
  },
];

export const TRANSLATOR_FORMS = [
  {
    id: 'load-video',
    name: 'Загрузка ролика',
  },
  {
    id: 'question-answer',
    name: 'Вопрос -ответ',
  },
  {
    id: 'auditory',
    name: 'Целевая аудитория',
  },
  {
    id: 'geolocation',
    name: 'Геолокация',
  },
  {
    id: 'show-time',
    name: 'Время показа',
  },
  {
    id: 'price',
    name: 'Стоимость',
  },
  {
    id: 'slider',
    name: 'Выбор обложки',
  },
];

export const AWARDS_NAME = {
  DISCOUNT: 'discount',
  CERTIFICATE: 'certificate',
  CASHBACK: 'cashback',
  PROMO_CODE: 'promoCode',
  PRIZE: 'prize',
  BLACK_BOX: 'blackBox',
  STOCK: 'stock',
};

export const AWARDS_RUS_NAME = {
  DISCOUNT: 'Дисконт',
  CERTIFICATE: 'Сертификат',
  CASHBACK: 'Кешбэк',
  PROMO_CODE: 'Промокод',
  PRIZE: 'Приз',
  BLACK_BOX: 'Черный ящик',
  STOCK: 'Акция',
};
