export default {
  // eslint-disable-next-line no-unused-vars
  install(Vue, options) {
    // Глобальный метод для роутинга
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$navigate = function (routeName) {
      this.$router.push({ name: routeName });
    };

    // Глобальный метод для проверки доступа пользователя
    // eslint-disable-next-line no-param-reassign
    Vue.prototype.$checkAccess = function (userAccess, currentAccess) {
      let result = false;
      if (!currentAccess.length) {
        return true;
      }

      userAccess.forEach((item) => {
        if (currentAccess.includes(item)) {
          result = true;
        }
      });
      return result;
    };
  },
};
