import Vue from 'vue';
import VueCookie from 'vue-cookie';
import VueToastify from 'vue-toastify';
import vueDebounce from 'vue-debounce';
import { ApolloClient, HttpLink, InMemoryCache } from 'apollo-boost';
import VueApollo from 'vue-apollo';
import commonPlugin from '@/plugins/common-plugin';
import App from './app.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import 'leaflet/dist/leaflet.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css';
import './styles/style.css';

const httpLink = new HttpLink({
  uri: 'https://cabinet.r2x.ru/graphql',
});

const apolloClient = new ApolloClient({
  link: httpLink,
  cache: new InMemoryCache(),
  connectToDevTools: true,
});

Vue.config.productionTip = false;
Vue.use(VueApollo);
Vue.use(VueCookie);
Vue.use(VueToastify, {
  position: 'top-right',
  theme: 'light',
});
Vue.use(commonPlugin);
Vue.use(vueDebounce);

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

new Vue({
  router,
  store,
  vuetify,
  apolloProvider,
  render: (h) => h(App),
}).$mount('#app');
