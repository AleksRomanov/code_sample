import { getId } from '@/utils';
import { ADV_USER_SCOPES, CONTENT_TYPES } from '@/const';

export const mockedCards = [
  {
    id: 1,
    type: CONTENT_TYPES.VIDEO,
    title: 'Название 1',
    questionsCount: 5,
    showTime: {
      dates: {
        from: '22.10.2022',
        to: '25.10.2022',
      },
      days: ['monday'],
      time: ['morning', 'evening'],
      showCount: 3,
    },
    location: ['Россия, Красноярский край, г. Красноярск'],
    auditory: {
      gender: ['male', 'female'],
      ages: ['20-30', '40-50'],
    },
  },
  {
    id: 2,
    type: CONTENT_TYPES.BANNER,
    title: 'Название 2',
    questionsCount: 5,
    showTime: {
      dates: {
        from: '22.10.2022',
        to: '25.10.2022',
      },
      days: ['monday'],
      time: ['morning', 'evening'],
      showCount: 3,
    },
    location: ['Россия, Красноярский край, г. Красноярск'],
    auditory: {
      gender: ['male', 'female'],
      ages: ['20-30', '40-50'],
    },
  },
  {
    id: 3,
    type: CONTENT_TYPES.AUDIO,
    title: 'Название 3',
    questionsCount: 5,
    showTime: {
      dates: {
        from: '22.10.2022',
        to: '25.10.2022',
      },
      days: ['monday'],
      time: ['morning', 'evening'],
      showCount: 3,
    },
    location: ['Россия, Красноярский край, г. Красноярск'],
    auditory: {
      gender: ['male', 'female'],
      ages: ['20-30', '40-50'],
    },
  },
  {
    id: 4,
    type: CONTENT_TYPES.BANNER,
    title: 'Название 4',
    questionsCount: 5,
    showTime: {
      dates: {
        from: '22.10.2022',
        to: '25.10.2022',
      },
      days: ['monday'],
      time: ['morning', 'evening'],
      showCount: 3,
    },
    location: ['Россия, Красноярский край, г. Красноярск'],
    auditory: {
      gender: ['male', 'female'],
      ages: ['20-30', '40-50'],
    },
  },
  {
    id: 5,
    type: CONTENT_TYPES.VIDEO,
    title: 'Название 5',
    questionsCount: 5,
    showTime: {
      dates: {
        from: '22.10.2022',
        to: '25.10.2022',
      },
      days: ['monday'],
      time: ['morning', 'evening'],
      showCount: 3,
    },
    location: ['Россия, Красноярский край, г. Красноярск'],
    auditory: {
      gender: ['male', 'female'],
      ages: ['20-30', '40-50'],
    },
  },
  {
    id: 6,
    type: CONTENT_TYPES.VIDEO,
    title: 'Название 6',
    questionsCount: 5,
    showTime: {
      dates: {
        from: '22.10.2022',
        to: '25.10.2022',
      },
      days: ['monday'],
      time: ['morning', 'evening'],
      showCount: 3,
    },
    location: ['Россия, Красноярский край, г. Красноярск'],
    auditory: {
      gender: ['male', 'female'],
      ages: ['20-30', '40-50'],
    },
  },
  {
    id: 7,
    type: CONTENT_TYPES.AUDIO,
    title: 'Название 7',
    questionsCount: 5,
    showTime: {
      dates: {
        from: '22.10.2022',
        to: '25.10.2022',
      },
      days: ['monday'],
      time: ['morning', 'evening'],
      showCount: 3,
    },
    location: ['Россия, Красноярский край, г. Красноярск'],
    auditory: {
      gender: ['male', 'female'],
      ages: ['20-30', '40-50'],
    },
  },
  {
    id: 8,
    type: CONTENT_TYPES.BANNER,
    title: 'Название 8',
    questionsCount: 5,
    showTime: {
      dates: {
        from: '22.10.2022',
        to: '25.10.2022',
      },
      days: ['monday'],
      time: ['morning', 'evening'],
      showCount: 3,
    },
    location: ['Россия, Красноярский край, г. Красноярск'],
    auditory: {
      gender: ['male', 'female'],
      ages: ['20-30', '40-50'],
    },
  },
];

export const mockedHistory = [
  {
    date: '26.03.2022',
    action: 'Опросник размещен',
    userName: 'Филиппова Ольга Андреевна',
    type: 'Активен',
  },
  {
    date: '24.03.2022',
    action: 'Удален опросник',
    userName: 'Конюхов Федов Иванович',
    type: 'Удален',
  },
  {
    date: '23.03.2022',
    action: 'Создан опросник',
    userName: 'Игнатьев Алексей Владимирович',
    type: 'Создан',
  },
  {
    date: '22.03.2022',
    action: 'Опросник помещен в архив',
    userName: 'Филиппова Ольга Андреевна',
    type: 'Архив',
  },
  {
    date: '21.03.2022',
    action: 'Создан опросник',
    userName: 'Филиппова Ольга Андреевна',
    type: 'Создан',
  },
  {
    date: '21.02.2022',
    action: 'Удален опросник',
    userName: 'Конюхов Федов Иванович',
    type: 'Удален',
  },
  {
    date: '19.02.2022',
    action: 'Опросник размещен',
    userName: 'Филиппова Ольга Андреевна',
    type: 'Активен',
  },
  {
    date: '20.01.2022',
    action: 'Опросник размещен',
    userName: 'Игнатьев Алексей Владимирович',
    type: 'Активен',
  },
  {
    date: '12.01.2022',
    action: 'Опросник размещен',
    userName: 'Конюхов Федов Иванович',
    type: 'Активен',
  },
  {
    date: '10.01.2022',
    action: 'Опросник помещен в архив',
    userName: 'Игнатьев Алексей Владимирович',
    type: 'Архив',
  },
  {
    date: '09.01.2022',
    action: 'Опросник размещен',
    userName: 'Филиппова Ольга Андреевна',
    type: 'Активен',
  },
  {
    date: '26.03.2022',
    action: 'Опросник размещен',
    userName: 'Филиппова Ольга Андреевна',
    type: 'Активен',
  },
  {
    date: '24.03.2022',
    action: 'Удален опросник',
    userName: 'Конюхов Федов Иванович',
    type: 'Удален',
  },
  {
    date: '23.03.2022',
    action: 'Создан опросник',
    userName: 'Игнатьев Алексей Владимирович',
    type: 'Создан',
  },
  {
    date: '22.03.2022',
    action: 'Опросник помещен в архив',
    userName: 'Филиппова Ольга Андреевна',
    type: 'Архив',
  },
  {
    date: '21.03.2022',
    action: 'Создан опросник',
    userName: 'Филиппова Ольга Андреевна',
    type: 'Создан',
  },
  {
    date: '21.02.2022',
    action: 'Удален опросник',
    userName: 'Конюхов Федов Иванович',
    type: 'Удален',
  },
  {
    date: '19.02.2022',
    action: 'Опросник размещен',
    userName: 'Филиппова Ольга Андреевна',
    type: 'Активен',
  },
  {
    date: '20.01.2022',
    action: 'Опросник размещен',
    userName: 'Игнатьев Алексей Владимирович',
    type: 'Активен',
  },
  {
    date: '12.01.2022',
    action: 'Опросник размещен',
    userName: 'Конюхов Федов Иванович',
    type: 'Активен',
  },
  {
    date: '10.01.2022',
    action: 'Опросник помещен в архив',
    userName: 'Игнатьев Алексей Владимирович',
    type: 'Архив',
  },
  {
    date: '09.01.2022',
    action: 'Опросник размещен',
    userName: 'Филиппова Ольга Андреевна',
    type: 'Активен',
  },
];

export const mockedQuestions = [
  {
    id: getId(),
    question: 'Есть ли у тебя секретная способность?',
    answer: 'Авто',
  },
  {
    id: getId(),
    question: 'Что делаешь дома, когда никого нет?',
    answer: 'Бизнес',
  },
  {
    id: getId(),
    question: 'О чем ты думаешь сейчас?',
    answer: 'Города и страны',
  },
  {
    id: getId(),
    question: 'Где и кем ты видишь себя через 5 лет?',
    answer: 'Гороскопы',
  },
  {
    id: getId(),
    question: 'А через 10 лет?',
    answer: 'Досуг',
  },
  {
    id: getId(),
    question: 'Есть ли у тебя секретная способность?',
    answer: 'Авто',
  },
  {
    id: getId(),
    question: 'Что делаешь дома, когда никого нет?',
    answer: 'Бизнес',
  },
  {
    id: getId(),
    question: 'О чем ты думаешь сейчас?',
    answer: 'Города и страны',
  },
  {
    id: getId(),
    question: 'Где и кем ты видишь себя через 5 лет?',
    answer: 'Гороскопы',
  },
  {
    id: getId(),
    question: 'А через 10 лет?',
    answer: 'Досуг',
  },
  {
    id: getId(),
    question: 'Есть ли у тебя секретная способность?',
    answer: 'Авто',
  },
  {
    id: getId(),
    question: 'Что делаешь дома, когда никого нет?',
    answer: 'Бизнес',
  },
  {
    id: getId(),
    question: 'О чем ты думаешь сейчас?',
    answer: 'Города и страны',
  },
  {
    id: getId(),
    question: 'Где и кем ты видишь себя через 5 лет?',
    answer: 'Гороскопы',
  },
  {
    id: getId(),
    question: 'А через 10 лет?',
    answer: 'Досуг',
  },
  {
    id: getId(),
    question: 'Есть ли у тебя секретная способность?',
    answer: 'Авто',
  },
  {
    id: getId(),
    question: 'Что делаешь дома, когда никого нет?',
    answer: 'Бизнес',
  },
  {
    id: getId(),
    question: 'О чем ты думаешь сейчас?',
    answer: 'Города и страны',
  },
  {
    id: getId(),
    question: 'Где и кем ты видишь себя через 5 лет?',
    answer: 'Гороскопы',
  },
  {
    id: getId(),
    question: 'А через 10 лет?',
    answer: 'Досуг',
  },
  {
    id: getId(),
    question: 'Есть ли у тебя секретная способность?',
    answer: 'Авто',
  },
  {
    id: getId(),
    question: 'Что делаешь дома, когда никого нет?',
    answer: 'Бизнес',
  },
  {
    id: getId(),
    question: 'О чем ты думаешь сейчас?',
    answer: 'Города и страны',
  },
  {
    id: getId(),
    question: 'Где и кем ты видишь себя через 5 лет?',
    answer: 'Гороскопы',
  },
  {
    id: getId(),
    question: 'А через 10 лет?',
    answer: 'Досуг',
  },
  {
    id: getId(),
    question: 'Есть ли у тебя секретная способность?',
    answer: 'Авто',
  },
  {
    id: getId(),
    question: 'Что делаешь дома, когда никого нет?',
    answer: 'Бизнес',
  },
  {
    id: getId(),
    question: 'О чем ты думаешь сейчас?',
    answer: 'Города и страны',
  },
  {
    id: getId(),
    question: 'Где и кем ты видишь себя через 5 лет?',
    answer: 'Гороскопы',
  },
  {
    id: getId(),
    question: 'А через 10 лет?',
    answer: 'Досуг',
  },
  {
    id: getId(),
    question: 'Есть ли у тебя секретная способность?',
    answer: 'Авто',
  },
  {
    id: getId(),
    question: 'Что делаешь дома, когда никого нет?',
    answer: 'Бизнес',
  },
  {
    id: getId(),
    question: 'О чем ты думаешь сейчас?',
    answer: 'Города и страны',
  },
  {
    id: getId(),
    question: 'Где и кем ты видишь себя через 5 лет?',
    answer: 'Гороскопы',
  },
  {
    id: getId(),
    question: 'А через 10 лет?',
    answer: 'Досуг',
  },
  {
    id: getId(),
    question: 'Есть ли у тебя секретная способность?',
    answer: 'Авто',
  },
  {
    id: getId(),
    question: 'Что делаешь дома, когда никого нет?',
    answer: 'Бизнес',
  },
  {
    id: getId(),
    question: 'О чем ты думаешь сейчас?',
    answer: 'Города и страны',
  },
  {
    id: getId(),
    question: 'Где и кем ты видишь себя через 5 лет?',
    answer: 'Гороскопы',
  },
  {
    id: getId(),
    question: 'А через 10 лет?',
    answer: 'Досуг',
  },
  {
    id: getId(),
    question: 'Есть ли у тебя секретная способность?',
    answer: 'Авто',
  },
  {
    id: getId(),
    question: 'Что делаешь дома, когда никого нет?',
    answer: 'Бизнес',
  },
  {
    id: getId(),
    question: 'О чем ты думаешь сейчас?',
    answer: 'Города и страны',
  },
  {
    id: getId(),
    question: 'Где и кем ты видишь себя через 5 лет?',
    answer: 'Гороскопы',
  },
  {
    id: getId(),
    question: 'А через 10 лет?',
    answer: 'Досуг',
  },
];
export const mockedRegions = [
  {
    name: 'Амурская область',
    value: 'Amurskaya',
  },
  {
    name: 'Архангельская область',
    value: 'Arhangelskaya',
  },
  {
    name: 'Астраханская область',
    value: 'Astrahansakya',
  },
  {
    name: 'Белгородская область',
    value: 'Belgorodskaya',
  },
  {
    name: 'Брянская область',
    value: 'Branskaya',
  },
  {
    name: 'Челябинская область',
    value: 'Chelabinskaya',
  },
  {
    name: 'Воронежская область',
    value: 'Voronezskaya',
  },
  {
    name: 'Иркутская область',
    value: 'Irkutskaya',
  },
  {
    name: 'Ивановская область',
    value: 'Ivanovskaya',
  },
  {
    name: 'Калининградская область',
    value: 'Kaliningradskaya',
  },
  {
    name: 'Калужская область',
    value: 'Kaluzskaya',
  },
  {
    name: 'Кемеровская область',
    value: 'Kemerovskaya',
  },
];

export const mockedTowns = [
  {
    name: 'Абакан',
    value: 'Abakan',
  },
  {
    name: 'Азов',
    value: 'Azov',
  },
  {
    name: 'Александров',
    value: 'Alexandrov',
  },
  {
    name: 'Алексин',
    value: 'Alexin',
  },
  {
    name: 'Альметьевск',
    value: 'Almetyevsk',
  },
  {
    name: 'Анапа',
    value: 'Anapa',
  },
  {
    name: 'Ангарск',
    value: 'Angarsk',
  },
  {
    name: 'Анжеро-Судженск',
    value: 'Anzero-Sujensk',
  },
  {
    name: 'Апатиты',
    value: 'Apatiti',
  },
  {
    name: 'Арзамас',
    value: 'Arzamas',
  },
  {
    name: 'Армавир',
    value: 'Armavir',
  },
  {
    name: 'Арсеньев',
    value: 'Arsenyev',
  },
];

export const getMockedThemesContent = () => ([
  {
    name: 'Счастье',
    value: 'Schastie',
  },
  {
    name: 'Богатство',
    value: 'Bogatstvo',
  },
  {
    name: 'Здоровье',
    value: 'Zdorovie',
  },
  {
    name: 'Спорт',
    value: 'Sport',
  },
  {
    name: 'Путешествия',
    value: 'Puteshestviya',
  },
  {
    name: 'hdfhfd',
    value: 'hdfhfd',
  },
  {
    name: '4444',
    value: '4444',
  },
  {
    name: '5555',
    value: '5555',
  },
  {
    name: '6666',
    value: '6666',
  },
  {
    name: '7777',
    value: '7777',
  },
]);

export const getMockedHistory = () => ([
  {
    id: 1,
    date: '2022-01-26',
    detail: 'Списание',
    price: '2000 бонусов',
  },
  {
    id: 2,
    date: '2022-01-27',
    detail: 'Списание',
    price: '2000 бонусов',
  },
  {
    id: 3,
    date: '2022-01-23',
    detail: 'Пополнение баланса',
    price: '2000 бонусов',
  },
  {
    id: 4,
    date: '2022-01-22',
    detail: 'Оплата клиенту',
    price: '2000 бонусов',
  },
  {
    id: 5,
    date: '2022-01-21',
    detail: 'Списание',
    price: '2000 бонусов',
  },
  {
    id: 6,
    date: '2022-01-29',
    detail: 'Списание',
    price: '2000 бонусов',
  },
  {
    id: 7,
    date: '2022-01-20',
    detail: 'Списание',
    price: '2000 бонусов',
  },
  {
    id: 8,
    date: '2022-02-26',
    detail: 'Списание',
    price: '2000 бонусов',
  },
  {
    id: 9,
    date: '2022-03-20',
    detail: 'Списание',
    price: '2000 бонусов',
  },
  {
    id: 10,
    date: '2022-01-07',
    detail: 'Пополнение баланса',
    price: '2000 бонусов',
  },
  {
    id: 11,
    date: '2022-03-13',
    detail: 'Оплата клиенту',
    price: '2000 бонусов',
  },
  {
    id: 12,
    date: '2022-04-11',
    detail: 'Списание',
    price: '2000 бонусов',
  },
  {
    id: 13,
    date: '2022-05-01',
    detail: 'Списание',
    price: '2000 бонусов',
  },
  {
    id: 14,
    date: '2022-02-02',
    detail: 'Списание',
    price: '2000 бонусов',
  },
]);

// export const notificationData = [
//   {
//     id: 1,
//     date: '26.01.2022',
//     notification: 'Добавлен сотрудник Филиппова Ольга Андреевна',
//   },
//   {
//     id: 2,
//     date: '26.01.2022',
//     notification: 'Добавлен сотрудник Филиппова Ольга Андреевна',
//   },
//   {
//     id: 3,
//     date: '26.01.2022',
//     notification: 'Добавлен сотрудник Филиппова Ольга Андреевна',
//   },
//   {
//     id: 4,
//     date: '26.01.2022',
//     notification: 'Добавлен сотрудник Филиппова Ольга Андреевна',
//   },
//   {
//     id: 5,
//     date: '26.01.2022',
//     notification: 'Добавлен сотрудник Филиппова Ольга Андреевна',
//   },
//   {
//     id: 6,
//     date: '26.01.2022',
//     notification: 'Добавлен сотрудник Филиппова Ольга Андреевна',
//   },
//   {
//     id: 7,
//     date: '26.01.2022',
//     notification: 'Добавлен сотрудник Филиппова Ольга Андреевна',
//   },
//   {
//     id: 8,
//     date: '26.01.2022',
//     notification: 'Добавлен сотрудник Филиппова Ольга Андреевна',
//   },
//   {
//     id: 9,
//     date: '26.01.2022',
//     notification: 'Добавлен сотрудник Филиппова Ольга Андреевна',
//   },
//   {
//     id: 10,
//     date: '26.01.2022',
//     notification: 'Добавлен сотрудник Филиппова Ольга Андреевна',
//   },
// ];

export const getMockedUsers = () => ([
  {
    id: getId(),
    fio: 'Иванов Петр Сергеевич',
    email: 'ivanov@mail.ru',
    phone: '89993586512',
    role: 'Менеджер по продажам',
    scopes: [ADV_USER_SCOPES.VIEW.VALUE],
  },
  {
    id: getId(),
    fio: 'Киселев Вадим Олегович',
    email: 'kiselev@mail.ru',
    phone: '89993582312',
    role: 'Главный бухгалтер',
    scopes: [ADV_USER_SCOPES.VIEW.VALUE, ADV_USER_SCOPES.ALL_SCOPES.VALUE],
  },
  {
    id: getId(),
    fio: 'Хорошко Сергей Александрович',
    email: 'horoshko@mail.ru',
    phone: '89996786512',
    role: 'Администратор',
    scopes: [ADV_USER_SCOPES.VIEW.VALUE, ADV_USER_SCOPES.ALL_SCOPES.VALUE],
  },
  {
    id: getId(),
    fio: 'Дуб Евпатий Агафонович',
    email: 'dub@mail.ru',
    phone: '89896386512',
    role: 'Начальник охраны',
    scopes: [ADV_USER_SCOPES.VIEW.VALUE, ADV_USER_SCOPES.ALL_SCOPES.VALUE],
  },
  {
    id: getId(),
    fio: 'Лучший Дмитрий Феликсович',
    email: 'best@mail.ru',
    phone: '89999999999',
    role: 'Директор',
    scopes: [ADV_USER_SCOPES.VIEW.VALUE, ADV_USER_SCOPES.ALL_SCOPES.VALUE],
  },
  {
    id: getId(),
    fio: 'Иванова Мила Александровна',
    email: 'ivanova@mail.ru',
    phone: '89999999999',
    role: 'Секретарь',
    scopes: [ADV_USER_SCOPES.VIEW.VALUE, ADV_USER_SCOPES.ALL_SCOPES.VALUE],
  },
]);

export const getMockedCompany = () => ({
  name: {
    short: 'УРАБИ',
  },
  inn: '2466277548',
  ogrn: '1142468061922',
  bank: '',
  bankBiK: '',
  bankAccount: '',
  correspondentCheck: '',
  email: '',
  phone: '',
  fio: 'Григоренко Александр Анатольевич',
  users: getMockedUsers(),
});

export const getMockedFunnelData = () => ({
  formName: 'funnel',
  id: getId(),
  href: {
    name: 'Ссылка на сайт',
    value: 'asdsadas',
  },
  questionCount: {
    name: 'Количество вопросов',
    value: 10,
  },
  questionName: {
    name: 'Название',
    value: 'dasdsadsad',
  },
  questions: {
    name: 'Вопросы',
    value: [
      {
        id: 788739749,
        question: 'dasdsad',
        answer1: 'sadsadsa',
        answer2: 'dsadsadsa',
        answer3: 'dsadsadasd',
      },
      {
        id: 542366048,
        question: 'asdasdsad',
        answer1: 'sadasdsad',
        answer2: 'sadasdsada',
        answer3: 'aaaaaaxcvcxvcxv',
      },
      {
        id: 299859563,
        question: 'xcvcxvtghgjhgj',
        answer1: 'dfdsfsdfdsf',
        answer2: 'bvnvbnvbnvb',
        answer3: 'yjtyjytjytjyt',
      },
      {
        id: 88773392,
        question: 'kjhkhjkhjkhj',
        answer1: 'hjkjhkhjkhjkhjk',
        answer2: 'hjkhjkhjkhjkhj',
        answer3: 'khbnmnbmnbmbnm',
      },
    ],
  },
  gender: {
    name: 'Пол',
    value: ['male', 'female'],
  },
  ages: {
    name: 'Возраст',
    value: {
      from: 18,
      to: 20,
    },
  },
  education: {
    name: 'Образование',
    value: 'неоконченное высшее',
  },
  income: {
    name: 'Доход',
    value: '30т-50т',
  },
  martialStatus: {
    name: 'Семейное положение',
    value: 'не женат(а)',
  },
  religion: {
    name: 'Религия',
    value: 'Иудаизм',
  },
  theme: {
    name: 'Тематика',
    value: ['family+', 'family', 'dog'],
  },
  geolocationName: {
    name: 'География показа',
    value: 'Abakan',
    lat: {
      name: 'Широта',
      value: 2132134,
    },
    lang: {
      name: 'Долгота',
      value: 2141251,
    },
  },
  timeOfTheDay: {
    name: 'Время показа',
    value: ['morning', 'day'],
  },
  price: {
    name: 'Стоимость',
    value: 10,
  },
  awards: {
    name: 'Награды',
    values: {
      discount: {
        awardsValue: 55,
        answerResult: 70,
      },
      certificate: {
        awardsValue: 'dsfdsf',
        answerResult: 65,
      },
      cashback: {
        awardsValue: 'Выбрать',
        answerResult: 'Правильных ответов',
      },
      promoCode: {
        awardsValue: '',
        answerResult: 'Правильных ответов',
      },
      prize: {
        awardsValue: '',
        answerResult: 'Правильных ответов',
      },
      blackBox: {
        awardsValue: '',
        answerResult: 'Правильных ответов',
      },
      stock: {
        awardsValue: '',
        answerResult: 'Правильных ответов',
      },
    },
  },
});

export const getFunnels = () => new Array(10).fill(null).map(() => getMockedFunnelData());

export const getMockedTowns = () => {
  const regions = mockedRegions.slice().map((item) => item.name);
  const towns = mockedTowns.slice().map((item) => item.name);
  return [...towns, ...regions];
};
